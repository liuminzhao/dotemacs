(setq load-path (cons "~/.emacs.d/" load-path))
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/")

(load "~/.emacs-basic.el")
(load "~/.emacs-theme.el")
(load "~/.emacs-package.el")
(load "~/.emacs-customize.el")

;; ; org in package.el
;; (unless package-archive-contents    ;; Refresh the packages descriptions
;;   (package-refresh-contents))
;; (setq package-load-list '(all))     ;; List of packages to load
;; (unless (package-installed-p 'org)  ;; Make sure the Org package is
;;   (package-install 'org))           ;; installed, install it if not
;; (package-initialize)                ;; Initialize & Install Package

;; ; org agenda setup
;; (setq org-agenda-files (quote ("~/Documents/NDNQI/"
;; 			       "~/Documents/bqrpt/")))

;; ;; ;;;;;;;;; yasnippet ;;;;;;;;;;
;; (require 'yasnippet) ;; not yasnippet-bundle
;; (yas-global-mode 1);
;; (setq yas/trigger-key "C-c y")
;; (setq yas-expand-from-trigger-key "C-c y")
;; (global-set-key (kbd "C-c y") 'yas/expand)
;; ;; (yas/initialize)
;; ;; (yas/load-directory "~/.emacs.d/yasnippet/snippets")
;; ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;; ;;;;;;;;; org ;;;;;;;
;; ;; ;(setq org-log-done t)
;; ;; (require 'org-install)
;; ;; (load "org-latex")

;; ;(add-hook 'markdown-mode-hook 'turn-on-cdlatex)
;; ;(add-hook 'LaTeX-mode-hook
;; ;          (let ((original-command (lookup-key LaTeX-mode-map [tab])))
;; ;            `(lambda ()
;; ;               (setq yas/fallback-behavior
;; ;                     '(apply ,original-command))
;; ;               (local-set-key [tab] 'yas/expand))))
;; ;(defun yas/advise-indent-function (function-symbol)
;; ;  (eval `(defadvice ,function-symbol (around yas/try-expand-first activate)
;; ;           ,(format
;; ;             "Try to expand a snippet before point, then call `%s' as usual"
;; ;             function-symbol)
;; ;           (let ((yas/fallback-behavior nil))
;; ;             (unless (and (interactive-p)
;; ;                          (yas/expand))
;; ;               ad-do-it)))))

;; ;(yas/advise-indent-function 'cdlatex-tab)
;; ;(yas/advise-indent-function 'org-cycle)
;; ;(yas/advise-indent-function 'org-try-cdlatex-tab)
;; ;(add-hook 'org-mode-hook 'yas/minor-mode-on)

;; ;; ; Anything

;; ;; (require 'anything)
;; ;; (require 'anything-config)
;; ;; (setq anything-sources
;; ;;       (list anything-c-source-buffers
;; ;;             anything-c-source-file-name-history
;; ;;             anything-c-source-info-pages
;; ;;             anything-c-source-man-pages
;; ;;                 anything-c-source-file-cache
;; ;;             anything-c-source-emacs-commands))
;; ;; (global-set-key (kbd "M-X") 'anything)

;; ;; (defun org-toggle-iimage-in-org ()
;; ;;   "display images in your org file"
;; ;;   (interactive)
;; ;;   (if (face-underline-p 'org-link)
;; ;;       (set-face-underline-p 'org-link nil)
;; ;;       (set-face-underline-p 'org-link t))
;; ;;   (iimage-mode))

;; ;; ; org-mode icon
;; ;; (setq load-path (cons "~/scripts/org-mode/org-icons/lisp" load-path))
;; ;; (setq org-startup-with-icons-mode t)

;; ;; ; conflict with ibus,
;; ;; (global-unset-key (kbd "C-SPC"))
;; ;; (global-set-key (kbd "C-M-SPC") 'set-mark-command)

;; ;; ;org-htmlslidy
;; ;; (require 'org-htmlslidy)

;; ; org
;; ;(setq org-reverse-note-order t)

;; ;; (custom-set-variables
;; ;;  '(org-reverse-note-order t))

;; ;; ;; the default flyspell behaviour
;; ;; (put 'LeTex-mode 'flyspell-mode-predicate 'tex-mode-flyspell-verify)

;; ;; ;; some extra flyspell delayed command
;; ;; (mapcar 'flyspell-delay-command	'(scroll-up1 scroll-down1))

;; ; ispell, aspell, cocoaspell, flyspell
;; ;; (setq ispell-program-name "aspell")
;; ;; (add-to-list 'exec-path "/usr/local/bin")

;; ;; (setq ispell-dictionary-alist
;; ;;       '((nil
;; ;; 	 "[A-Za-z]" "[^A-Za-z]" "[']" nil
;; ;; 	 ("-B" "-d" "en_US.multi" "--dict-dir"
;; ;; 	  "/Users/liuminzhao/Downloads/usr/local/lib/aspell-0.60")
;; ;; 	 nil iso-8859-1)))

;; ;; (if (file-executable-p "/usr/local/bin/aspell")
;; ;;      (progn
;; ;;        (setq ispell-program-name "/usr/local/bin/aspell")
;; ;;        (setq ispell-extra-args '("-d" "/Library/Application Support/
;; ;; cocoAspell/aspell6-en-6.0-0/en.multi"))
;; ;;        ))

;; ; org-mobile
;; ;; (setq org-mobile-directory "~/Dropbox/MobileOrg")
;; ;; (setq org-directory "~/Documents/bqrpt/")
;; ;; (setq org-mobile-inbox-for-pull "~/tmp/agenda.org")

;; ; emacs for python https://github.com/gabrielelanaro/emacs-for-python/
;; ;(add-to-list 'load-path "~/tmp/emacs-for-python")
;; ;(load-file "~/tmp/emacs-for-python/epy-init.el")
;; ;(epy-setup-ipython)
;; ;(global-hl-line-mode t) ;; To enable
;; ;(require 'highlight-indentation)
;; ;(add-hook 'python-mode-hook 'highlight-indentation)

;; ; unicode correction
;; ;; (if (string-equal system-type "darwin")
;; ;;     (set-fontset-font "fontset-default"
;; ;;                       'unicode
;; ;;                       '("Monaco" . "iso10646-1")))

;; ; breqn and dmath
;; ; <http://tex.stackexchange.com/questions/22573/how-can-i-tell-auctex-that-breqn-is-a-math-environment>
;; ; http://stackoverflow.com/questions/1578127/how-do-i-break-a-long-equation-over-lines
;; ;; (add-hook 'LaTeX-mode-hook 'add-my-latex-environments)
;; ;; (defun add-my-latex-environments ()
;; ;;   (LaTeX-add-environments
;; ;;      '("dmath" LaTeX-env-label)))

;; ;; Code 'I' added to make syntax highlighting work in Auctex

;;  ;;Stops putting {} on argumentless commands to "save" whitespace

;; ;; Additionally, reftex code to recognize this environment as an equation
;; (setq reftex-label-alist
;;   '(("dmath" ?e nil nil t)))

;; ;; helm
;; (global-set-key (kbd "C-c h") 'helm-mini)

;; ;; latexmk

;; (add-hook 'LaTeX-mode-hook (lambda ()
;;   (push
;;     '("Latexmk" "latexmk -pdf %s" TeX-run-TeX nil t
;;       :help "Run Latexmk on file")
;;     TeX-command-list)))

;; '("%(-PDF)"
;;   (lambda ()
;;     (if (and (not TeX-Omega-mode)
;;              (or TeX-PDF-mode TeX-DVI-via-PDFTeX))
;;         "-pdf" "")))

;; (set-face-background 'git-gutter:modified "purple") ;; background color
;; (set-face-foreground 'git-gutter:added "green")
;; (set-face-foreground 'git-gutter:deleted "red")
