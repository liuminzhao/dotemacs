;; ; theme in emacs 24 <http://batsov.com/articles/2012/02/19/color-theming-in-emacs-reloaded/>

;; ;; random-theme by flonk
;; (defun random-theme()
;;   (interactive)
;;   (dolist (theme custom-enabled-themes)
;;     (disable-theme theme))
;;   (load-theme (let ((theme (nth (random (length (custom-available-themes)))
;; (custom-available-themes))))
;;                 (message "loaded theme: %s" theme)
;;                 theme)))
;; (condition-case nil
;;     (random-theme)
;;   (error nil))

;; M-x load-theme
;; (load-theme 'tango-dark t)
;; (load-theme 'misterioso t)
(load-theme 'wombat t)
;; (load-theme 'tango t)
;; ; (load-theme 'solarized-light t)
;; ;; (load-theme 'wombat t)
;; (load-theme 'solarized-dark t)
;; ;; (load-theme 'wheatgrass t)

;; ;; sublime-text-2 theme <https://gist.github.com/k-bx/2907481>
;; (defun sublime-text-2 ()
;;   (interactive)
;;   (color-theme-install
;;    '(sublime-text-2
;;       ((background-color . "#171717")
;;       (background-mode . light)
;;       (border-color . "#1a1a1a")
;;       (cursor-color . "#fce94f")
;;       (foreground-color . "#cfbfad")
;;       (mouse-color . "black"))
;;      (fringe ((t (:background "#1a1a1a"))))
;;      (mode-line ((t (:foreground "#eeeeec" :background "#555753"))))
;;      (region ((t (:foreground "#404040" :background "#CC9900"))))
;;      (font-lock-builtin-face ((t (:foreground "#52e3f6"))))
;;      (font-lock-comment-face ((t (:foreground "#ffffff"))))
;;      (font-lock-function-name-face ((t (:foreground "#edd400"))))
;;      (font-lock-keyword-face ((t (:foreground "#ff007f"))))
;;      (font-lock-string-face ((t (:foreground "#ece47e"))))
;;      (font-lock-type-face ((t (:foreground"#8ae234"))))
;;      (font-lock-variable-name-face ((t (:foreground "#8ae234"))))
;;      (minibuffer-prompt ((t (:foreground "#729fcf" :bold t))))
;;      (font-lock-warning-face ((t (:foreground "Red" :bold t))))
;;      )))
;; (provide 'sublime-text-2)
;; (call-interactively 'sublime-text-2)
