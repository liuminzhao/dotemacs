;; emacs customized function

;; ; presnell's align tabular
(defun LaTeX-align-current-environment ()
  (interactive)
  (point-to-register ?P)
  (LaTeX-mark-environment)
  (align-current)
  (jump-to-register ?P))
(add-hook 'LaTeX-mode-hook
  (lambda ()
   (define-key LaTeX-mode-map "\C-c\C-a" 'LaTeX-align-current-environment)))

; plug in day and month by fangzhzh
(defun my-insert-date ()
    (interactive)
;    (insert "//")
;    (insert (user-full-name))
;    (insert "@")
;    (insert (format-time-string "%Y/%m/%d %H:%M:%S" (current-time))))
    (insert (format-time-string "%Y-%m-%d" (current-time))))
(global-set-key (kbd "C-c m d") 'my-insert-date)

(defun my-insert-time ()
    (interactive)
   (insert (format-time-string "%Y-%m-%d %H.%M.%S" (current-time))))
(global-set-key (kbd "C-c m t") 'my-insert-time)

;; select above lines
(defun select-above-lines ()
  "Select the above lines"
  (interactive)
  (set-mark (point-min)))

;; markdown table from orgtbl
(defun markdown-regexp-right (beg end)
  (interactive "r")
  (replace-regexp "-\\+-" "-|-" nil beg end)
)

;; http://stackoverflow.com/questions/23636226/how-to-round-all-the-numbers-in-a-region
;; round number
(defun my-round-nb (start end)
  "round the nb of the region."
  (interactive "r")
  (save-restriction
    (narrow-to-region start end)
    (goto-char 1)
    (let ((case-fold-search nil))
      (while (search-forward-regexp "\\([0-9]+\\.[0-9]+\\)" nil t)
	(replace-match (format "%0.1f" (string-to-number (match-string 1)))
		       )))))

;; macro: est(sd)
(fset 'estsd
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("\346(\346\346)\346" 0 "%d")) arg)))

(global-set-key (kbd "C-c f") 'estsd)
