(show-paren-mode t)
(display-time-mode 1)
(setq display-time-24hr-format t)
(setq display-time-day-and-date t)
(setq system-time-locale "C")
(setq display-time-interval 10);
(setq column-number-mode t)
(setq line-number-mode t)
(setq show-paren-style 'parenthesis)
(setq mouse-avoidance-mode 'animate)
(setq frame-title-format "%b[%f]@emacs")
(global-set-key [?\C-z] 'undo)
(setq calendar-location-name "Austin")
(global-font-lock-mode t)
(auto-image-file-mode t)
(fset 'yes-or-no-p 'y-or-n-p)
(setq x-select-enable-clipboard t)
(setq user-full-name "Minzhao Liu")
(setq user-mail-address "liuminzhao@ufl.edu")
(when (display-graphic-p)
  (toggle-scroll-bar -1)
  (tool-bar-mode -1))
(setq time-stamp-active t)
(setq time-stamp-warn-inactive t)
(setq time-stamp-format "%:u %02m/%02d/%04y %02H:%02M:%02S")
(add-hook 'write-file-hooks 'time-stamp) ; update when saving
(setq resize-mini-windows t)
(blink-cursor-mode -1)
(transient-mark-mode 1)
;(mouse-wheel-mode t)
(setq visible-bell nil)
;(set-scroll-bar-mode 'nil) ; no scroll bar
;(setq scroll-step 1
;  scroll-margin 3
;  scroll-conservatively 10000)
(setq inhibit-startup-message t)
(setq kill-ring-max 200)
(setq apropos-do-all t)
;; (setq-default auto-fill-function 'do-auto-fill)
;; (partial-completion-mode 1)
;; (setq-default ispell-program-name "ispell")
;; (setq ispell-program-name "/usr/local/Cellar/ispell/3.3.02/bin/ispell")
(setq exec-path (append exec-path '("/usr/local/bin")))

;; emacs latex not working
(getenv "PATH")
(setenv "PATH"
	(concat
	 "/usr/texbin" ":"

	 (getenv "PATH")))
(setenv "PATH"
	(concat
	 "/usr/local/bin" ":"

	 (getenv "PATH")))

;; desktop
(desktop-save-mode 1)

;; ido
(ido-mode t)

;; ibuffer
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; ; put all ## file together
(setq backup-directory-alist
      '((".*" . "~/.emacs.d/backups")))

;; delete whitespace

(add-hook 'before-save-hook (lambda () (delete-trailing-whitespace)))

;; ;;;;;;;;; window move ;;;;;;;;
(global-set-key [M-left] 'windmove-left)          ; move to left windnow
(global-set-key [M-right] 'windmove-right)        ; move to right window
(global-set-key [M-up] 'windmove-up)              ; move to upper window
(global-set-key [M-down] 'windmove-down)          ; move to downer window

;; encoding system
(prefer-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

;; easier buffer killing
(global-set-key (kbd "M-k") 'kill-this-buffer)

;; ;; font size <http://stackoverflow.com/questions/294664/how-to-set-the-font-size-in-emacs>
;; (set-face-attribute 'default nil :height 120)

;; ; initial agenda list
;; ; (add-hook 'after-init-hook 'split-window-horizontally)
;; (add-hook 'after-init-hook 'org-agenda-list)

;; chinese
(set-frame-font "Menlo-15")
(set-fontset-font
    (frame-parameter nil 'font)
    'han
    (font-spec :family "Hiragino Sans GB" ))
