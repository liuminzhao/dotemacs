(require 'cl-lib)

(require 'package)
;; (add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
;; (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))

; Adjusting load-path after updating packages
;; too slow
;; (defun package-update-load-path ()
;;   "Update the load path for newly installed packages."
;;   (interactive)
;;   (let ((package-dir (expand-file-name package-user-dir)))
;;     (mapc (lambda (pkg)
;;             (let ((stem (symbol-name (car pkg)))
;; 		  (version "")
;; 		  (first t)
;; 		  path)
;; 	      (mapc (lambda (num)
;; 		      (if first
;; 			  (setq first nil)
;; 			  (setq version (format "%s." version)))
;; 		      (setq version (format "%s%s" version num)))
;; 		    (aref (cdr pkg) 0))
;;               (setq path (format "%s/%s-%s" package-dir stem version))
;;               (add-to-list 'load-path path)))
;;           package-alist)))

;; git-gutter
;; (global-git-gutter-mode +1)
;; (setq git-gutter:added-sign "+ ")
;; (setq git-gutter:deleted-sign "- ")
;; (setq git-gutter:modified-sign "= ")

;; Auctex, install in elpa, and no need further config
;; (add-to-list 'load-path "~/.emacs.d/elpa/auctex-11.87.2")
;; (load "auctex.el" nil t t)
;; (load "preview-latex.el" nil t t)

;; ; rainbow-delimiters
;; (require 'rainbow-delimiters)
;; (global-rainbow-delimiters-mode)

; powerline
(require 'powerline)
; change powerline color
(setq powerline-color1 "grey22")
(setq powerline-color2 "grey40")
(setq powerline-arrow-shape 'arrow14)

;; golden-ratio
;; (require 'golden-ratio)
;; (golden-ratio-enable)

; magit
(add-to-list 'load-path "/Users/liuminzhao/.emacs.d/elpa/magit-20130621.140")
(require 'magit)
;; magit key binding
(global-set-key [(control f9)] (lambda () (interactive) (magit-status default-directory)))
(global-set-key [(f9)] (lambda () (interactive) (magit-status default-directory)))


; change default magit diff color
(eval-after-load 'magit
  '(progn
     (set-face-foreground 'magit-diff-add "green3")
     (set-face-foreground 'magit-diff-del "red3")
     (when (not window-system)
       (set-face-background 'magit-item-highlight "black"))))

;; full screen magit-status from Sveen

(defadvice magit-status (around magit-fullscreen activate)
  (window-configuration-to-register :magit-fullscreen)
  ad-do-it
  (delete-other-windows))

(defun magit-quit-session ()
  "Restores the previous window configuration and kills the magit buffer"
  (interactive)
  (kill-buffer)
  (jump-to-register :magit-fullscreen))

(define-key magit-status-mode-map (kbd "q") 'magit-quit-session)


;; multiple cursor
(add-to-list 'load-path "/Users/liuminzhao/.emacs.d/elpa/multiple-cursors-20130518.816/")
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;; expand-region
(add-to-list 'load-path "/Users/liuminzhao/.emacs.d/elpa/expand-region-20130509.9")
(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

;; ;; Auctex
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(setq TeX-PDF-mode t)

(setq TeX-view-program-list '(("Skim" "open -a Skim %o"))) ; use evince to open pdf
(setq TeX-view-program-selection '((output-pdf "Skim")))

(setq LaTeX-command-style '(("" "%(PDF)%(latex) -file-line-error %S%(PDFout)"))) ; for C-c ` fail

;; ; markdown mode
(autoload 'markdown-mode "markdown-mode"
  "Major mode for editing Markdown files" t)
(setq auto-mode-alist
      (cons '("\\.md" . markdown-mode) auto-mode-alist))

(setq auto-mode-alist
      (cons '("\\.Rmd" . markdown-mode) auto-mode-alist))

(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))

;; (global-set-key (kbd "C-i") 'markdown-cycle) ; tab not working for cycle under terminal

;; ; iimage
;; (autoload 'iimage-mode "iimage" "Support Inline image minor mode." t)
;; (autoload 'turn-on-iimage-mode "iimage" "Turn on Inline image minor mode." t)
;; ;; (add-to-list 'iimage-mode-image-regex-alist
;; ;;              (cons (concat "\\[\\[file:\\(~?" iimage-mode-image-filename-regex
;; ;;                            "\\)\\]")  1))

;; ess
;; (add-to-list 'load-path "/Users/liuminzhao/.emacs.d/elpa/ess-20131022.1636/lisp/")
(add-to-list 'load-path "/Users/liuminzhao/.emacs.d/ess-13.09-1/lisp/")
(load "ess-site")
;; (setq ess-use-auto-complete t)
(require 'ess-jags-d)
(add-to-list 'auto-mode-alist '("\\.jag\\'" . jags-mode))
(add-to-list 'auto-mode-alist '("\\.bug\\'" . jags-mode))

;; undo-tree
(add-to-list 'load-path "~/.emacs.d/elpa/undo-tree-20130516.8")
(require 'undo-tree)
(global-undo-tree-mode)

;; flyspell
(require 'flyspell)
(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)

;; latex-pretty-symbol (melpa)
;; (add-to-list 'load-path "~/.emacs.d/elpa/latex-pretty-symbols-20111011.1446/")
;; (require 'latex-pretty-symbols)

;;;;; cd-latex ;;;;
(autoload 'cdlatex-mode "cdlatex" "CDLaTeX Mode" t)
(autoload 'turn-on-cdlatex "cdlatex" "CDLaTeX Mode" nil)

(add-hook 'LaTeX-mode-hook 'turn-on-cdlatex)   ; with AUCTeX LaTeX mode
(add-hook 'org-mode-hook 'turn-on-org-cdlatex)

;; auto-complete
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
(ac-config-default)

;; (setq ac-auto-start 3)
;; (define-key ac-complete-mode-map "\M-[" 'ac-next)
;; (define-key ac-complete-mode-map "\M-]" 'ac-previous)
;; (global-auto-complete-mode t)

;; elpy
(package-initialize)
(elpy-enable)

;; ediff
(setq ediff-split-window-function 'split-window-horizontally)

;; ido-vertical-mode
(add-to-list 'load-path "/Users/liuminzhao/.emacs.d/elpa/ido-vertical-mode-20131209.938/")
(require 'ido-vertical-mode)
(ido-mode 1)
(ido-vertical-mode 1)

;; yasnippet (elpa)
(add-to-list 'load-path "/Users/liuminzhao/.emacs.d/elpa/yasnippet-20140106.1009/")
;; (require 'yasnippet)
(autoload 'yas-global-mode "yasnippet" "yasnippet Global Mode" t)
;; (yas-global-mode 1)

;; ;; ; quick-jump.el
(require 'quick-jump)
(quick-jump-default-keybinding)

;; org-mode
(setq org-todo-keywords
       '((sequence "TODO(t)" "WAIT(w@/!)" "|" "DONE(d!)" "CANCELED(c@)")))

;; ;; electric pair
(electric-pair-mode 1)

;; ;; org-reveal
(require 'ox-reveal)
(setq org-reveal-root "file:///Users/liuminzhao/dev/reveal.js")

;; outline-minor-mode <http://www.emacswiki.org/emacs/OutlineMinorMode>
;; Outline-minor-mode key map
(define-prefix-command 'cm-map nil "Outline-")
;; HIDE
(define-key cm-map "q" 'hide-sublevels)    ;; Hide everything but the top-level headings
(define-key cm-map "t" 'hide-body)         ;; Hide everything but headings (all body lines)
(define-key cm-map "o" 'hide-other)        ;; Hide other branches
(define-key cm-map "c" 'hide-entry)        ;; Hide this entry's body
(define-key cm-map "l" 'hide-leaves)       ;; Hide body lines in this entry and sub-entries
(define-key cm-map "d" 'hide-subtree)      ;; Hide everything in this entry and sub-entries
;; SHOW
(define-key cm-map "a" 'show-all)          ;; Show (expand) everything
(define-key cm-map "e" 'show-entry)        ;; Show this heading's body
(define-key cm-map "i" 'show-children)     ;; Show this heading's immediate child sub-headings
(define-key cm-map "k" 'show-branches)     ;; Show all sub-headings under this heading
(define-key cm-map "s" 'show-subtree)      ;; Show (expand) everything in this heading & below
;; MOVE
(define-key cm-map "u" 'outline-up-heading)                ;; Up
(define-key cm-map "n" 'outline-next-visible-heading)      ;; Next
(define-key cm-map "p" 'outline-previous-visible-heading)  ;; Previous
(define-key cm-map "f" 'outline-forward-same-level)        ;; Forward - same level
(define-key cm-map "b" 'outline-backward-same-level)       ;; Backward - same level
(global-set-key "\M-o" cm-map)

;; helm
(require 'helm-R)
(helm-mode 1)
(global-set-key (kbd "C-c h") 'helm-mini)
